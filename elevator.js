/* Elevator project */
const events = require('events');
const eventEmitter = new events.EventEmitter();

class Elevator {
  constructor(name, range) {
    this.name = name;
    this.range = range;
    this.current = 0;
    this.step = null;
    this.requestUp = [];
    this.requestDown = [];
    this.isGoingUp = true;
    this.isDoorOpen = false;
  }

  addStopDown(number) {
    console.log(`${this.name}:\t Add stop \t floor: ${number}`);
    this.requestDown.push(number);
  }

  addStopUp(number) {
    console.log(`${this.name}:\t Add stop \t floor: ${number}`);
    this.requestUp.push(number);
  }

  move() {
    console.log(`Start moving ${this.name}`);
    this.step = setInterval(async () => {

      if (this.isDoorOpen) {
        return;
      }

      if (this.isGoingUp) {
        if (this.requestUp.length == 0) {
          this.isGoingUp = false;
          return;
        };
        this.current = this.current + 1;
        console.log(`${this.name}:\t is on \t\t floor ${this.current}`);

        const isStop = this.requestUp.includes(this.current);
        if (isStop) {
          this.requestUp = this.requestUp.filter(floor => floor != this.current);
          await this.openDoor();
          await this.closeDoor();
        }
      } else {
        // is going donw
        if (this.requestDown.length == 0) {
          this.isGoingUp = true;
          return;
        };
        this.current = this.current - 1;
        console.log(`${this.name}:\t is on \t\t floor ${this.current}`);

        const isStop = this.requestDown.includes(this.current);
        if (isStop) {
          this.requestDown = this.requestDown.filter(floor => floor != this.current);
          await this.openDoor();
          await this.closeDoor();
        }
      }
      const topOrBottom = this.current == this.range[0] || this.current == this.range[1];
      if (topOrBottom) {
        this.isGoingUp = !this.isGoingUp;
      }

    }, 1000)
  }

  pressNumber(number) {
    if (number < this.range[0] || number > this.range[1]) {
      console.error(`${this.name}:\t does not exist \t floor ${number}`);
      return -1;
    }
    console.log(`${this.name}:\t press for \t floor: ${number}`);
    if (this.isGoingUp) {
      const isOnTheWayUp = this.current < number;
      if (isOnTheWayUp) {
        this.addStopUp(number);
      } else {
        this.addStopDown(number);
      }
    } else {
      const isOnTheWayDown = number < this.current;
      if (isOnTheWayDown) {
        this.addStopDown(number);
      } else {
        this.addStopUp(number);
      }
    }
  }

  openDoor() {
    console.log(`${this.name}:\t opening doors`);
    this.isDoorOpen = true;
  }

  closeDoor() {
    console.log(`${this.name}:\t closing doors`);
    this.isDoorOpen = false;
  }

  emergency() {
    clearInterval(this.step);
    console.log(`${this.name}:\t Emergency`);
    this.openDoor();
  }

  reset() {
    this.closeDoor();
    this.move();
  }
}

// test Elevator
// const elevatorA = new Elevator('A', [-1,9])
// elevatorA.openDoor();
// elevatorA.closeDoor();
// elevatorA.move();
// elevatorA.pressNumber(9)
// elevatorA.pressNumber(3)
// elevatorA.pressNumber(5)
// elevatorA.pressNumber(-1)
// elevatorA.pressNumber(10) // log error
// setTimeout(() => {
// elevatorA.emergency();
//   elevatorA.reset();
// }, 3000);
// setTimeout(() => {
//   elevatorA.pressNumber(5)
// }, 25000)

function control() {

  const elevatorA = new Elevator('A', [-1,9]);
  const elevatorB = new Elevator('B', [0,10]);
  elevatorA.reset();
  elevatorB.reset();

  eventEmitter.on('ping', () => eventEmitter.emit('pong'));
  eventEmitter.on('whereA', () => eventEmitter.emit('whereA:ok', elevatorA.current));
  eventEmitter.on('whereB', () => eventEmitter.emit('whereB:ok', elevatorB.current));

  eventEmitter.on('up', (floor) => {
    const onTheWayOfElevatorA = elevatorA.isGoingUp && floor > elevatorA.current;
    if (onTheWayOfElevatorA) {
      elevatorA.addStopUp(floor);
      return;
    }

    const onTheWayOfElevatorB = elevatorB.isGoingUp && floor > elevatorB.current;
    if (onTheWayOfElevatorB) {
      elevatorB.addStopUp(floor);
      return;
    }

    elevatorA.addStopDown(floor) // could be any
  })

  eventEmitter.on('down', (floor) => {
    const onTheWayOfElevatorA = !elevatorA.isGoingUp && floor < elevatorA.current;
    if (onTheWayOfElevatorA) {
      return elevatorA.addStopDown(floor);
    }

    const onTheWayOfElevatorB = !elevatorB.isGoingUp && floor < elevatorB.current;
    if (onTheWayOfElevatorB) {
      return elevatorB.addStopDown(floor);
    }

    return elevatorrB.addStopUp(floor) // could be any
  })

  eventEmitter.on('pressNumber', ({ number, name }) => {
    if (name == 'A') {
      return elevatorA.pressNumber(number);
    }

    if (name == 'B') {
      return elevatorB.pressNumber(number);
    }
    console.error(`Name ${name} is not A nor B`);
    return -1;
  })

  eventEmitter.on('emergency', ({ name }) => {
    if (name == 'A') {
      return elevatorA.emergency();
    }

    if (name == 'B') {
      return elevatorB.emergency();
    }
    console.error(`Name ${name} is not A nor B`);
    return -1;
  });

  eventEmitter.on('reset', ({ name }) => {
    if (name == 'A') {
      return elevatorA.reset();
    }

    if (name == 'B') {
      return elevatorB.reset();
    }
    console.error(`Name ${name} is not A nor B`);
    return -1;
  });

}

// RUNNING EXAMPLE

control();

eventEmitter.emit('up', 4);

setTimeout(() => {
  eventEmitter.emit('up', 1);
}, 3000)

setTimeout(() => {
  eventEmitter.emit('pressNumber', ({ number: 3, name: 'A' }));
}, 5000)

setTimeout(() => {
  eventEmitter.emit('pressNumber', ({ number: 10, name: 'B' }));
}, 5000)

setTimeout(() => {
  eventEmitter.emit('emergency', ({ name: 'B' }));
}, 9000)
